pipeline {
  agent any
  
  parameters {

    string(name: 'SLACK_CHANNEL_1',
           description: 'Default Slack channel to send messages to',
           defaultValue: '#genaral')

    string(name: 'SLACK_CHANNEL_2',
           description: 'Default Slack channel to send messages to',
           defaultValue: '#genaral')           

  } // parameters
  
  
  stages {
    stage('Compile') {
      steps {
      slackSend (
                 tokenCredentialId: 'slack_token',
                 color: "${env.SLACK_COLOR_GOOD}",
                 channel: "${params.SLACK_CHANNEL_1}",
                 message: "*SUCCESS:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${env.USER_ID}\n More info at: ${env.BUILD_URL}")

        sh './gradlew assembleRelease'
      }
    }

    stage('Unit test') {
      steps {
        sh './gradlew testDebugUnitTest testDebugUnitTest'
      }
    }

    stage('Build APK') {
      steps {
        withCredentials(bindings: [file(credentialsId: 'appdistribution_id', variable: 'GOOGLE_APPLICATION_CREDENTIALS')]) {
          sh 'echo $GOOGLE_APPLICATION_CREDENTIALS'
          sh './gradlew assembleRelease appDistributionUploadRelease'
        }

        archiveArtifacts '**/*.apk'
      }
    }

    stage('Static analysis') {
      steps {
        sh './gradlew lintDebug'
      }
    }

    stage('Deploy') {
      when {
        branch 'master'
      }
      environment {
        SIGNING_KEYSTORE = credentials('my-app-signing-keystore')
        SIGNING_KEY_PASSWORD = credentials('my-app-signing-password')
      }
      post {
        success {
          mail(to: 'beta-testers@example.com', subject: 'New build available!', body: 'Check it out!')
        }

      }
      steps {
        sh './gradlew assembleRelease'
        archiveArtifacts '**/*.apk'
        androidApkUpload(googleCredentialsId: 'Google Play', apkFilesPattern: '**/*-release.apk', trackName: 'beta')
      }
    }

  }
  post {
    failure {
      mail(to: 'android-devs@example.com', subject: 'Oops!', body: "Build ${env.BUILD_NUMBER} failed; ${env.BUILD_URL}")
    }

  }
  options {
    skipStagesAfterUnstable()
  }
}